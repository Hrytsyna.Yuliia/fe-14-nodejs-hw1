const fs = require('fs');
const express = require('express');
const router = express.Router();
const { getFiles, checkFolder } = require('./helpers');

router.post('/', (req, res) => {
    const { filename, content } = req.body;
    if (!req.body.filename || !req.body.content) {
        res.status(400).json({ message: "Please specify required parameters" });
    } else {
        if (getFiles('./uploads').indexOf(req.body.filename) >= 0) {
            res.status(400).json({ message: `File "${req.body.filename}" already exist.` });
        } else {
            let extension = req.body.filename.split('.');
            if (['log', 'txt', 'json', 'yaml', 'xml', 'js'].indexOf(extension[extension.length - 1]) >= 0 && extension.length > 1) {
                checkFolder('./uploads');
                fs.appendFile(`./uploads/${req.body.filename}`, req.body.content, (err) => {
                    if (err) throw err;
                    res.status(200).json({ message: `File "${req.body.filename}" created successfully.` });
                });
            } else {
                res.status(400).json({ message: `Wrong extention.` });
            }
        }

    }
});

router.get('/', (req, res) => {
    let files = getFiles('./uploads');
    if (files.length === 0) {
        res.status(400).json({ message: "No files found." });
    } else {
        res.status(200).json({ message: "Success.", files: files });
    }

});

router.get('/:filename', function (req, res) {
    let allFiles = getFiles('./uploads');
    let fileName = req.params.filename;
    if (allFiles.indexOf(fileName) >= 0) {
        let content = fs.readFile(`./uploads/${fileName}`, (err) => {
            if (err) throw error;
            let extension = req.params.filename.split('.');
            let date = fs.statSync(`./uploads/${fileName}`);
            res.status(200).json({
                message: "Success.",
                filename: req.params.filename,
                content: content,
                extension: extension[extension.length - 1],
                uploadedDate: date.birthtime
            });
        });
    } else {
        res.status(400).json({ message: `File '${fileName}' not found.` });
    }
});

router.delete('/:filename', (req, res) => {
    let files = getFiles('./uploads');
    let fileName = req.params.filename;
    if (files.indexOf(fileName) >= 0) {
        fs.unlink(`./uploads/${fileName}`, (err) => {
            if (err) throw error;
            res.status(200).json({ message: `File "${fileName}" deleted.` });
        });
    } else {
        res.status(400).json({ message: `File '${fileName}' not found.` });
    }

});

router.put('/:filename', (req, res) => {
    let files = getFiles('./uploads');
    let fileName = req.params.filename;
    const { content } = req.body;
    if (files.indexOf(fileName) >= 0) {
        if (req.body.content) {
            fs.appendFile(`./uploads/${fileName}`, req.body.content, (err) => {
                if (err) throw error;
                res.status(200).json({ message: `File "${fileName}" was updated.` });
            });
        } else {
            res.status(400).json({ message: `Please specify required parameters.` });
        }

    } else {
        res.status(400).json({ message: `File '${fileName}' not found.` });
    }

});

module.exports = router;
