const fs = require('fs');
const path = require('path');

const getFiles = (dir, list = []) => {
    if (!fs.existsSync('uploads')) {
        fs.mkdirSync('uploads');
    }
    let filesList = list;
    let files = fs.readdirSync(dir);
    for (let i in files) {
        let filePath = path.join(dir, files[i]);

        let extension = files[i].split('.');
        if (files[i] !== 'node_modules') {
            if (fs.statSync(filePath).isFile()) {
                if (['log', 'txt', 'json', 'yaml', 'xml', 'js'].indexOf(extension[extension.length - 1]) >= 0) {
                    filesList.push(files[i]);
                }
            } else {
                getFiles(`${filePath}`, filesList);
            }
        }

    }

    return filesList
};

const checkFolder = (folderPath) => {
    if (!fs.existsSync(folderPath)) {
        fs.mkdirSync(folderPath);
    }
}

module.exports = { getFiles, checkFolder };
