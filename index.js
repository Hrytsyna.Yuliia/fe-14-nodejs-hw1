const express = require('express');
const app = express();
const listRouter = require('./router');

app.use(express.json());
app.use('/api/files', listRouter);
app.use((err, req, res, next) => {
    res.status(500).send('500: Internal server error');
});

app.listen(8080, () => {
    console.log('Server works');
});